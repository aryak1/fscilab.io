---
title: "About Us"
date: 2020-06-13T17:13:37+05:30
author: "false"
draft: false
translationKey: about
---
### **Free Software Community of India is a collective of Free Software\* users, advocates and developers.**
<br>

- We maintain communication and collaboration infrastructure for everyone that respects their freedom and privacy.
- We depend on donations and volunteers to run the infrastructure.
- We maintain a list of Free Software communities and offer sub domains of https://fsug.in or https://fosscommunity.in
- We mentor Free Software enthusiasts to become Free Software contributors.
- We provide GNU/Linux installation and configuration support via online messaging groups.

<a id="clarification"></a>
=======
>\* *Free Software is often confused with Open Source Software by those who are not aware of the difference between them. Free Software or Libre Software deals with user freedom and their rights primarily, where as Open-Source only cares about the pragmatic benefits of having a public code base that anyone can contribute to. We care about our rights and freedom hence we prefer to use Free Software and therefore our community is called the Free Software Community of India. Learn more about Free Software [here](https://www.gnu.org/philosophy/free-sw.html).*
