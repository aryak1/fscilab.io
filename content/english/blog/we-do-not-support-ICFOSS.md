---
title: "We do not support ICFOSS anymore !"
date: 2021-07-13T03:47:36+05:30
author: "false"
tags: ["icfoss"]
discussionlink: https://codema.in/d/VAEbKIoJ/response-to-icfoss-director-sabarish-k-s-requests-to-community-/49
draft: false
---

International Centre for Free and Open Source Software (ICFOSS) is an 
organization started to promote FOSS initiatives in Kerala linking up 
with FOSS and OSHW (Open Source Hardware) communities from different 
parts of the world. We, as FSCI and as individual members supported
past ICFOSS initiatives. 

Once a thriving organization who worked with Free Software and Free Software
communities now have drifted away from their main goal for the past several
years and their initiatives related to Free Software trimmed down to doing
nothing. These days, ICFOSS is just another research organization eating public
funds, alarmingly who don't even know the basic difference between freeware and
free software. Yet, we raised our various concerns to them in both offline and
online meetings on multiple occasions, all such attempts turned out to be just
plain waste of our time.

FSCI is deeply disappointed that the ICFOSS has neither addressed any concerns
raised, nor acknowledge the fact that there were concerns and blindly ask for
wholehearted support from the communities. We feel our efforts are going in vain
trying to support them. Hence, It's better to stay away from this toxic
organization. And we request other free software communities to do the same.

We decided, NOT to join or support ICFOSS in any of their programs. We 
only plan to revisit our decision once ICFOSS complete the promises 
that we mutually agreed and respond to our queries which we put 
forward based on their request.

(https://codema.in/d/VAEbKIoJ/response-to-icfoss-director-sabarish-k-s-requests-to-community-/49)