---
title: "Donate today to keep our services running"
date: 2022-01-20T23:14:02+05:30
draft: false 
tags: ["crowdfunding", "fundraising", "donation", "community"]
discussionlink: https://codema.in/d/VkCybRTY/fsci-services-accounting
---
We believe every computer user must have freedom and privacy. A lot of services on the internet are privacy-invading and snoop on their users. Therefore, as ethical replacements, to bring the change we seek, [we run services](/#poddery) so that people can use them without the fear of being tracked or having to spend their time and effort in maintaining them. 

 Our services are open to everyone, such as Jitsi Meet for video/audio calls, Encrypted Chat Services, etc. and these services are administered and maintained by trusted and skilled volunteers at FSCI in their free time .

But running these services comes along with their own costs. The costs mainly include renting computers connected to the internet(servers) that run these services, associated  storage, purchasing domain names and sometimes hiring people outside the community to fix certain issues that our own volunteers are unable to fix.

Mainstream corporate services are funded by selling user data. We do not collect any personal information about our users. We rely on donations to cover the monetary costs of running these services. But we [do not receive sufficient donations](https://codema.in/d/VkCybRTY/fsci-services-accounting) to cover these costs. As a result, some of the community members end up paying from their pockets to keep the services up and running. Although, we are trying our best to keep these services running, we think that only a few members covering the whole cost is unfair to them and not sustainable in the long term. 

We saw great efforts by many volunteers and financial contributors in our Jitsi Meet crowdfunding in which we targeted ₹ 62,500 for 2 years and [15 people donated to cover 57% of the target](https://jitsi-fund.fsci.in/contributors/) in one year. The donations ranged from ₹ 10 to ₹ 16440. This shows us the potential of people coming together and contributing in whatever ways they can for the things they care about. Donations go a long way!

<b>Please visit [this page](https://fund.fsci.in/donate/) to make financial contributions to support our free software powered and privacy-respecting services. Every contribution counts, whatever be the amount.</b>

In addition to financial contributions, we are always looking for volunteers to help maintain our services. It does not matter who you are or how technically skilled you are, only the willingness to learn and contribute is enough. Other than financial contributions or volunteering for maintaining the services, there are a lot of ways you can help us in the free software movement at FSCI, please check out this page. In addition, you can contribute in other ways-- making posters, contribute by writing articles campaigning for free software on our website, monitor funding status of various services and organize crowd funding campaigns when funds are depleted, raise awareness about free software, privacy, organizing events and talks, by designing posters, banners etc.

<b>We are grateful to all the contributors.</b>
