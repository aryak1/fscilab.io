---
title: "Open Letter to teachers of Kerala"
date: 2021-08-29T23:15:31+05:30
author: "false"
draft: false
tags: ["schools", "kerala", "teachers", "google", "g-suite", "classrooms", "education", "videoconference"]
discussionlink: https://codema.in/d/u3EDIs4x/kerala-schools-choose-proprietary-google-suite-for-online-teaching
---

**TL;DR**: Google's free-of-cost offer of G-Suite to Kerala schools is a scheme to make more customers for G-Suite, retain their monopoly power in the ad business and search engines. Dependence on Google can lead to massive surveillance ([and its chilling effects](https://socialcooling.com)), losing our political agency to Google, [and many others](https://www.gnu.org/proprietary/malware-google.html). We urge educational institutes to reject this offer and switch to [free software](https://ravidwivedi.in/posts/free-sw/) (freedom-respecting software or swatantra software) for all the activities. For network-based services, we urge institutes to self-host free software powered services. With self-hosting and using free software, the software and the data will be under the institute's control.

**Please [sign this letter](#kobo-form) if you support free software in education.**

### KITE announces G-Suite in classrooms

We are deeply saddened to learn that [The Kerala Infrastructure and Technology for Education (KITE) has rolled out the G-Suite platform](https://www.thehindu.com/news/national/kerala/kite-rolls-out-g-suite-platform-for-online-classes-in-kerala/article35217726.ece) for all the online classroom activities in schools in Kerala. G-Suite platform is a collection of Google services like, Google Meet, Google Drive, Gmail, Google Docs etc. Read the official announcement [here](https://kite.kerala.gov.in/KITE/itsadmin/uploads/docs/604.pdf).

### Promise of Privacy vs Reality

KITE also announced, "No sensitive individual details of students or teachers will be shared. KITE will have the master control of data uploaded in the platform." Providing Google accounts with a randomized student and teacher ID won't protect their privacy from Google because [Google collects a lot of data on each user](https://www.theguardian.com/commentisfree/2018/mar/28/all-the-data-facebook-google-has-on-you-privacy), and it can easily combine all this data to deanonymize students and teachers.

Google uses cross-site tracking: When you browse from site to site, you’re often followed by trackers that collect data on where you’ve been and what you’ve done, using scripts, widgets or even tiny, invisible images embedded on the sites you visit. Take, for example, those social share buttons embedded on many websites. Sites may choose to include those buttons to gain useful analytics about their content, but the buttons also send data back to the social platforms. Sometimes, that makes sense, allowing you to share content on other social platforms. But often, that data also ends up being used behind the scenes to target advertising or create user profiles.

Google Analytics is a web analytics service offered by Google that tracks and reports users visiting on a website. In order to block Google Analytics, users are required to use extra protection in their browser, for example, by using ublock-origin add-on. This leads us to ask KITE if they can turn off Google Analytics in the G-Suite.

### How are ads shown by Google different from the ads you see on cable TV?

The way Google shows ads is fundamentally different to the way cable TV or radio shows ads. Google uses user's personal data to target ads while the TV, radio or newspaper ads are not based on user's personal data as the same ad is being watched by everyone reading the newspaper or watching the same channel on TV. Since, the ads that Google provides are based on user's behavior, search history, webpages they visited, their emails, their purchases, articles they read etc. and therefore have a lot more power to influence the users.

This offer by Google is just a scheme to gain more users (captive audience to whom they can target ads) which will give Google a lot of data on students. This will reinforce the dominance of Google in the ad-business. They already use their dominance to eliminate the competition. Google uses this data to manipulate their users with ads. A company with so much data as Google can [even influence elections](https://www.politico.com/magazine/story/2015/08/how-google-could-rig-the-2016-election-121548/). For example, Facebook is a company with a [lot of data on its users](https://www.theguardian.com/commentisfree/2018/mar/28/all-the-data-facebook-google-has-on-you-privacy), which was used by Cambridge Analytica [to influence elections](https://en.wikipedia.org/wiki/Facebook%E2%80%93Cambridge_Analytica_data_scandal) a few years ago.

As an example, Google's search can amplify the voices of people it agrees with and dampen those of people it disagrees with, effectively manipulating the public opinion.

### Why Google is giving it for free-of-cost?

Google's free of cost offer is similar to Jio's free-of-cost offer of internet connection, which later started charging money. As a result of Jio's offer, Vodafone and Idea had to merge because they couldn't compete, and we are left with a lesser number of mobile service providers. There is no guarantee that Google's services will remain free-of-cost forever. They can use their position in the educational suite provider or videoconferencing to eliminate competition and then raise prices at a later stage. Google Meet might become a dominant platform for video calls, eliminating all the competition like Zoom. (Note that Zoom is a nonfree software which we don't endorse in any way, it was mentioned just as an example).

### Effects of massive surveillance

Massive surveillance has pretty adverse effects on the society and individuals. What's at stake is our political liberty, freedom of speech, privacy, security, social equity,

- Statistics show that [lack of privacy leads to a population who is afraid to ask questions or educate themselves](https://theintercept.com/2016/04/28/new-study-shows-mass-surveillance-breeds-meekness-fear-and-self-censorship/), even if the issues are important and the motives are pure.

- Massive surveillance has [adverse effects on mental health](https://theprint.in/theprint-valuead-initiative/surveillance-a-massive-challenge-for-mental-health-so-india-needs-robust-data-protection-laws/512430/). These include insomnia, social anxiety, depression, hypervigilance, specific situational type phobias, post-traumatic stress disorder (PTSD), agoraphobia, obsessive-compulsive disorder (OCD), and many other issues.

- Surveillance puts us at risk of abuses by those in power, even if we’re doing nothing wrong at the time of surveillance.

We’d like to quote some points from this excellent resource about effects of massive surveillance at [Social Cooling](https://socialcooling.com).

- “If you feel you are being watched, you change your behavior.” This could limit your desire to take risks or exercise free speech. Over the long term, these ‘chilling effects’ could ‘cool down’ society. Rating systems can create unwanted incentives, and increase pressure to conform to a bureaucratic average. When doctors in New York were given scores, [this had unexpected results](https://science.slashdot.org/story/15/07/22/1752209/giving-doctors-grades-has-backfired). Doctors that tried to help advanced cancer patients had a higher mortality rate, which translated into a lower score. Doctors that didn’t try to help were rewarded with high scores, even though their patients died prematurely.

- People are starting to realize that this ‘digital reputation’ could limit their opportunities:

- [You may not get that dream job if your data suggests you’re not a very positive person](https://www.theguardian.com/science/2016/sep/01/how-algorithms-rule-our-working-lives).

- [If you are a woman, you may see fewer ads for high paying jobs](https://www.theguardian.com/technology/2015/jul/08/women-less-likely-ads-high-paid-jobs-google-study).

- [If you have “bad friends” on social media, you might pay more for your loan](https://trustingsocial.com/).

- [Tinder’s algorithms might not show you attractive people if you are not desirable yourself](https://www.fastcompany.com/3054871/whats-your-tinder-score-inside-the-apps-internal-ranking-system).

- [If you return goods to the store often, this will be used against you](https://apprissretail.com/).

- [What you post on social media may influence your odds of getting a tax audit](https://news.slashdot.org/story/17/08/29/2333209/the-irs-decides-who-to-audit-by-data-mining-social-media).

- [Your health insurer may collect intimate data about your lifestyle, race and more](https://www.propublica.org/article/health-insurers-are-vacuuming-up-details-about-you-and-it-could-raise-your-rates).

### What's the solution?

We need to control the software that we use. Freedom-respecting software means users have freedom to run, study, modify, share the software. If a software respects all these freedoms, we call it free software. Free Software is a matter of liberty, not price. To emphasize that point, we also call it swatantra software. Example of free software are Firefox, VLC, Thunderbird, Ubuntu etc.  If any of the above-mentioned freedoms is lacking, we call it nonfree/proprietary software. Examples of nonfree software are- WhatsApp, Microsoft Windows, Google Meet, Adobe Reader etc. For internet-based services like videoconferencing, online file storage, emails etc., the users (in this case, KITE or Kerala Government) must have the freedom to offer the same services on their own servers. This practice is known as self-hosting. These freedoms ensure that the users control the software and data. With proprietary software, we are at the mercy of the developer of the software, for example, in G-Suite, we are at the mercy of Google.

As an example, The British East India company created railway tracks, roads and bridges free-of-cost, and it needs no mention of what happened after that. So dependence usually results in enslavement or exploitation.

People might think that they are not programmers, and they don't need the freedom to study and modify the program. But we exercise the freedom to inspect and modify all the time, for example, we call plumber to fix our water taps, electrician for electricity-related problems, mechanics for repairing our bikes, cars etc. Similarly, individually or collectively (for example, KITE or Kerala Government),  we must be able to fix or adapt software with help of companies like Infosys, Wipro etc.

G-Suite software is nonfree software and so users don't control it, and [educational institutes should use only free software](https://fsf.org.in/article/education/).

A free software named [Tux Paint](http://directory.fsf.org/wiki/TuxPaint) used at VHSS Irimpanam school, Kerala, where 11 and 12 years old students completely translated the program's interface to Malayalam, and added pictures of local flowers along with recordings of Malayalam pronunciations of their name to the program's library. While doing this, they exercised their freedom to learn how the program works and modify the program, which demonstrates that even non programmers or children, [can actually directly improve software](https://www.gnu.org/education/edu-software-tuxpaint.html) when software freedom is granted.

Scribus is a cross-platform (works on GNU/Linux, Microsoft Windows, macOS, etc) Free Software for Desktop Publishing (replacement for software like Adobe PageMaker and InDesign) which initially only had support for Latin languages. It’s the developers of a community project funded by the Oman Government, called HOST-Oman (House of Open Source Technologies - Oman), who introduced support for Non-Latin languages with Complex Text Layout in Scribus. And as a part of this development, Malayalam also got supported in Scribus. So we can see anyone improving a software is beneficial to all the users even if they don't improve it directly. This enabled [Janayugom and Deshabhimani newspapers to shift to 100% Free Software for their publishing](https://poddery.com/posts/4691002).

### Replacement Software:

Following are the free software that can be used in classrooms and should replace their proprietary counterparts:

- Operating system: GNU/Linux distros like Debian, Ubuntu.

- Online Classes: Jitsi, BigBlueButton.

- Instant Messenger: Element, Quicksy, Conversations.

- Uploading videos: PeerTube

- E-learning platform: Moodle, BBB

- Recording Lectures: OBS

- Sharing notes, lecture videos etc. : Nextcloud, Lufi

- Digital writing pad: Xournal

- Document editor: LibreOffice, Cryptpad, Etherpad, EtherCalc, OnlyOffice

- Form filling: KoBoToolbox, NextCloud Forms — Disroot provides NextCloud Forms.

### Call for Action

We would like to remind the teachers from Kerala [how you stood for Software Freedom and became a model for the entire world](http://www.space-kerala.org/files/Story%20of%20SPACE%20and%20IT%40School.pdf). You resisted offers of free of cost training by Microsoft and Intel. You created a custom GNU/Linux distribution with software you needed in schools, and provided technical support and training to teachers across Kerala. You can do that again and lead the world in this changed scenario as well. We urge teachers from Kerala to join us in this [campaign to talk to KITE and push for adoption of free software based learning platforms for online classes](https://fsf.org.in/news/proprietary-apps-in-schools/).

**Please sign the letter below if you support the campaign.**


<div id="kobo-form" style="height: 100vh">
    <iframe frameborder="2" src="https://ee.kobotoolbox.org/single/lAHoPCiT" width="100%" height="100%" style="margin: auto;" ?returnUrl=""></iframe>
</div>

<br>

Last Updated on October 30, 2021
### Signatories

**Teachers from Kerala**

1. A K Francis
2. Shameel Abdulla
3. Anitha.C.K
4. Jobin
> കേരളം ഒന്ന് മാറി ചിന്തിക്കട്ടെ!

**Teachers from Outside Kerala**

1. Snehal Shekatkar
> The educational entities need to realize that the convenience they seem to be getting through the use of proprietary software like g-suite comes at a huge cost. We have a collective responsibility to stand for our digital freedom. Use only freedom respecting software when you teach students.
2. Benjamin Slade

**Others**

1. Vivek K J
2. Rahul Raj
3. Amal Vinod
4. Shrini
5. Nithin C
6. ബിജീഷ് മോഹൻ റ്റി
> സൗജന്യവും സ്വതന്ത്രവുമായ സോഫ്റ്റ്‌വെയറുകൾ തന്നെ കഴിവതും ഉപയോഗിക്കുക.
7. Ajose Joseph
8. Mujeeb Rahman K
> kerala schools already using foss. dont go back to proprietary solutions. i can help with any kind of training
9. Amrithanath
> "Free software and its philosophy should taught to every students regardless of their subjects. Philosophy of FLOSS is  such a great one and learning it not only help them to create better software which respects the user but also hopefully enables them to create a better society which respects evryone and shares everything, participate in activities etc.
> But using proprietary closed source software which doesn't care about users and only treat them as product shouldn't be the main mode communication they should have"
10. Madhav V
> "KITE has all the capability to implement a free at the same time efficient e-learning platform using existing technologies. If they do so, it will be a revolution. Please dont rely on freebies from big internet companies."
11. Pranav K
> There are lot of free and open source alternatives that can be used by the teachers and students for their privacy and security.
12. Abhishek
> "great job guys , this is a great step you guys have taken . also consider teaching them kids about free softwares a bit"
13. George Martin Jose: 
> I support FOSS. FOSS FTW
14. Sajesh Cherian
> Google doesn't have our best interest at heart. When they provide their suite for free, it only serves to train people in how to use Google software and become a slave to their data collection practice. We can do better. Please reconsider your decision.
15. Julin
> Going into the arms of google is easy, getting out of there would be much more difficult. Better not get entrapped.
16. Subin P T
> A decision shouldn't be rushed in to just because it is an easy option. Its drawbacks and the alternates should always be considered.
17. അഭിനവ് കൃഷ്ണ സി കെ
18. Jyothis Jagan
19. Abraham Raji
> Education liberates people hence it only makes sense that educational institutions should not use use tools that are designed to implant dependence. Just as schools say no to substance abuse they should say no to proprietary software.
20. Kailas E K
21. Dr. V. Sasi Kumar
22. Sheikh Shakil Akhtar
23. Aravind P Unnithan
24. മുരളി ഐ പി
25. സിജു
26. Harikrishnan A
> This is a great cause in this age and I'm happy to help with any setup required.
27. T D
> Free software is the only way for schools and districts to be autonomous and ensure the rights of their students is respected. Students should not be subject to mass surveillance by Google and other tech giants just to get an education. Free software teaches students how to be independent and learn from others who have put in the work to solve hard problems. Furthermore, it puts the school in full control over their technology stack, which can be adapted and refined over time. Additionally, it can result in cost savings. G-Suite does none of the above and exists solely to mine data from students to fuel Google's dominance. The public sector is paid for by citizens; this money should not fall into the hands of an unethical private entity.
28. Aman Das
> KITE can and should self host the infrastructure using FOSS software. Education is a public service and should be based on public infrastructure. Also the CS curriculum should promote FOSS software.
29. Jinu Raju
30. Ranjith Siji
31. Roshan More