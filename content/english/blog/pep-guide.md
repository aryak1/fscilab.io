---
title: "Email encryption guide"
date: 2021-07-15
author: "false"
tags: ["encryption", "pep"]
draft: false
discussionlink: https://codema.in/d/RvzuOf2X/guide-for-encrypting-mails-using-pep
---

This is a guide to encrypt emails using [pep app](https://pep.security). Pep is available for download for [Android](https://www.pep.security/en/android/), [iOS](https://www.pep.security/en/ios/), and for desktop, it is available as an [add-on in Thunderbird](https://pep.software/thunderbird/).

#### Guide to set up pep in Android :

For Android, pep app is available on [F-Droid](https://f-droid.org) and Google Play Store. We recommend you to download the app from F-Droid because F-Droid is a freedom-respecting software.

**Step 1** :  Download the pep app in Android. 

**Step 2** : The app will ask you for the permission to read contacts and download files. Permission to read contacts is to autofill the contacts when writing mails and the permission to download files is to save attachments in your phone. Both permissions are optional and can be changed at a later time. 

**Step 3** : Set up your email account by entering email account and password. In case you have a Google Account, select “Use OAuth 2.0 token”. 

**Step 4** : Depending on your email provider, you might have to manually enter IMAP/POP and SMTP details. These details would usually be available on the website of your email provider. 

**Step 5** : Check the box 'Store messages securely for all accounts' which means that pep will store encrypted mails on the server of your mail provider in encrypted form. pep has explained this feature [here](https://www.pep.security/docs/general_information.html#store-messages-securely).

**Step 6** : Set an account name and a display name for outgoing mails.

Congratulations, you pep account is set up.

pep app will generate private keys.

If you already have gpg keys, you can import your private keys to pep app.

The first mail from a pep user will be unencrypted, but when you reply to that mail, you can check the 'Privacy Status' of that user which should be yellow. This means that the message will be end-to-end encrypted.

In order to mitigate MITM attacks, pep has introduced a feature called [handshake](https://www.pep.security/docs/general_information.html#handshake). You can do this by pressing the Privacy Status icon and then comparing the Trustwords using other means of communication. Select "Confirm" if the Trustwords match, otherwise select "Reject". Now you will see a green icon on the Privacy Status of the contact you handshaked with. It says 'Secure & Trusted'.

From now on, all the future messages to this contact will be encrypted with GPG. This means that your email provider cannot read your mails as well as the subject of the mail (as they are encrypted with your private keys). The email provider will still have metadata-- whom you contacted you and when. But sending encrypted mails is better for privacy than sending unencrypted mails.

<br>

#### Guide to set up pep in iOS :

**Step 1** : Install pep app in iOS from the [App Store](https://apps.apple.com/us/app/pep-email-with-encryption/id1375458631).

**Step 2** : After installing the app, open the app and select account type: iCloud, Google, Outlook etc. If you email provider is not listed, then click on 'Other'.

**Step 3** : Enter email account details-- email address, password and other fields.

Congratulations, you email account in pep is now ready to encrypt mails. 

Now, when you receive the first mail from a pep user, it will be unecnrypted. When you reply to this mail, the Privacy Status of this contact will be yellow which means email will be send encrytped. 

Next, compare the trust words obtained from the Privacy Status of the contact with them using any other communication channel. When you confirmed the Trustwords, you can see that the Privacy Status is green. Now all the mails to this contact will be encrypted by default.

**Warning**

Importing already existing password-protected gpg keys to pep Android app will cause some problems. It will ask you for password on launching the app, pep app remembers it for 10 minutes. If another encrypted mail arrives after that, it can't remember password or ask again. So it won't show any new mails until you manually kill (force close) the app and open again. We [reported the issue](https://pep.community/t/support-using-open-keychain-directly-via-api-for-gpg-keys/86/) but it is not fixed yet. 

If you are already using gpg keys, then be prepared to import them in pep without password. 
 
Thunderbird pep plugin works well and does not have the same problem. 

**Contact Us**

If you have any queries/comments/suggestions, feel free to drop an email at admin at fsci dot in.
