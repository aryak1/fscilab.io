## Contact Us


Volunteers can contribute to FSCI in various ways, listed below: 

Technical contributions:

- Programming/Coding
- System Administration for the different services we run
- Debian Packaging of services that use native packages. Many services we run uses native debian packages, so contributing to maintaining these packages help us run our services well.
- Provide technical support in our online groups when people ask for help.

Non-technical contributions:

- Contribute by writing articles campaigning for free software on our website.
- Monitor funding status of various services and organize crowd funding campaigns when funds are depleted.
- Raise awareness about free software, privacy etc.
- Organizing events and talks.
- By designing posters, banners etc.

Financial contributions:

- By [donating](https://fund.fsci.in) to [services](/#poddery) run by FSCI.

To contact or volunteer for us, please fill the contact form below. 

<div id="kobo-form" style="height: 100vh">
<iframe frameborder="2" src="https://ee.kobotoolbox.org/single/fhs90AD4" width="100%" height="100%" style="margin: auto;"     ?returnUrl=""></iframe>
</div>

For any queries, please send emails to contact at fsci dot in.

You can also reach out to us via matrix, xmpp or IRC, mentioned in the footer of this page. 
