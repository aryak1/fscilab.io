# Ongoing Initiatives/Campaigns

The following initiatives are ongoing as part of FSCI.

- Outreach Team: Our outreach team is to talk to people outside of the community, such as talking to people who write to us. Since communication with people is very important to build a movement, outreach is a very important part of our activities. Let us know by sending an email to contact at fsci dot in if you would like to get involved in the outreach team.

- [Software Freedom Camp Diversity Edition 2021](https://camp.fsci.in): This is an initiative by us to teach people about free software philosophy, help them in contributing to free software projects and involving them in our community. In the 2021 edition, we focused on people from underprivileged backgrounds increase diversity in the community. Even when we are not doing the camp, we are always looking for new volunteers. 

- Open Letter to Kerala Teachers: This campaign aims to convince the educational institutes of Kerala to switch to Free Software, and was a response to KITE accepting Google's free-of-cost offer of G-Suite into the classrooms. [Sign the letter](/blog/letter-to-kerala-teachers/) today. 

- Promoting Free Software in education: This campaign aims to convince educational institutes to switch to free software and provide technical help. Educational institutes interested in switching to free software, please contact us by writing an email to contact at fsci dot in. Join for discussion: 

	- [Mailing list](https://lists.fsci.in/postorius/lists/fs-edu.fsci.in/).
	
	- Web-based [collaborative decision making group](https://codema.in/free-software-community-of-india-fsci-free-software-for-education/).

	- [XMPP](https://conversations.im/j/fs-edu@groups.poddery.com).

	- [Matrix](https://matrix.to/#/#fs-edu:poddery.com).

- Fixing the matrix-XMPP bifrost bridge: Matrix-XMPP bifrost bridge has a bug that [XMPP users miss messages](/blog/xmpp-matrix-bridge/) sent in the group when they were offline when the group is hosted on matrix. We are working on fixing this. Please read more on the [opencollective page of the project](https://opencollective.com/bifrost-mam). Please donate to the project if you would like to help fixing this.

### How to Get Involved

Visit our [contact page](/contact) if you want to get involved in any of these campaigns. Or you can check other options to [join us](#join-us). You can also write an email to contact at fsci dot in to get involved in these campaigns.
